import { makeStyles, Card, CardContent, IconButton } from "@material-ui/core";
import { Quote } from "../graphql/types";
import ArrowDropUpIcon from "@material-ui/icons/ArrowDropUp";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import { useState } from "react";

const useStyles = makeStyles({
  root: {
    minWidth: 275,
    margin: "1rem",
  },
  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)",
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
  cardGrid: {
    display: "grid",
    gridTemplateColumns: "auto 15%",
  },
  votesContainer: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
  },
  vote: {
    margin: "0 auto",
  },
});

export default function QuoteCard(props: {
  quote: Quote;
  vote: (id: number, increment: number) => void;
}) {
  const { quote, vote } = props;
  const [votes, setVotes] = useState(quote.votes);
  const classes = useStyles();
  return (
    <Card className={classes.root}>
      <CardContent>
        <div className={classes.cardGrid}>
          <div>
            <h2>{quote.quote}</h2>
            <p>
              <strong>{quote.character}</strong> - {quote.episode}
            </p>
          </div>
          <div className={classes.votesContainer}>
            <IconButton
              onClick={() => {
                vote(quote.id, 1);
                setVotes(votes + 1);
              }}
            >
              <ArrowDropUpIcon />
            </IconButton>
            <div className={classes.vote}>{votes}</div>
            <IconButton
              onClick={() => {
                vote(quote.id, -1);
                setVotes(votes - 1);
              }}
            >
              <ArrowDropDownIcon />
            </IconButton>
          </div>
        </div>
      </CardContent>
    </Card>
  );
}
