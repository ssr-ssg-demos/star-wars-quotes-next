import Link from "next/link";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import {
  IconButton,
  Typography,
  makeStyles,
  Theme,
  createStyles,
  Menu,
  MenuItem,
  styled,
} from "@material-ui/core";
import MoreIcon from "@material-ui/icons/MoreVert";
import React from "react";
import Router from "next/router";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
      a: {
        color: "inherit",
        textDecoration: "inherit",
      },
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    },
  })
);

const PlainLink = styled("a")({
  color: "inherit",
  textDecoration: "inherit",
});

const Header = () => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  return (
    <AppBar position="static">
      <Toolbar>
        <Typography
          variant="h6"
          className={classes.title}
          onClick={() => Router.push("/")}
        >
          NextJS
        </Typography>
        <IconButton
          aria-label="display more actions"
          edge="end"
          color="inherit"
          onClick={handleClick}
        >
          <MoreIcon />
        </IconButton>
        <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={handleClose}
        >
          <MenuItem>
            <Link href="/quotes">
              <PlainLink>All Quotes</PlainLink>
            </Link>
          </MenuItem>
          <MenuItem>
            <Link href="/">
              <PlainLink>Vote</PlainLink>
            </Link>
          </MenuItem>
        </Menu>
      </Toolbar>
    </AppBar>
  );
};

export default Header;
