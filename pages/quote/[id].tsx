import Layout from "../../components/layout";
import { makeStyles, Card, CardContent, Typography } from "@material-ui/core";
import fetch from "node-fetch";

const useStyles = makeStyles({
  card: {
    margin: "5rem 5rem auto",
  },
  scroll: {
    overflow: "scroll",
    maxHeight: 500,
  },
});

export default function Quote({ quote }) {
  const classes = useStyles();
  return (
    <Layout>
      <Card className={classes.card}>
        <CardContent>
          <h1>{quote.quote}</h1>
          <h2>{quote.character}</h2>
          <h3>{quote.episode}</h3>
          <div className={classes.scroll}>
            <Typography color="textSecondary">
              Did you ever hear the tragedy of Darth Plagueis The Wise? I
              thought not. It’s not a story the Jedi would tell you. It’s a Sith
              legend.
              <br />
              <br />
              Darth Plagueis was a Dark Lord of the Sith, so powerful and so
              wise he could use the Force to influence the midichlorians to
              create life… He had such a knowledge of the dark side that he
              could even keep the ones he cared about from dying. The dark side
              of the Force is a pathway to many abilities some consider to be
              unnatural.
              <br />
              <br />
              He became so powerful… the only thing he was afraid of was losing
              his power, which eventually, of course, he did.
              <br />
              <br />
              Unfortunately, he taught his apprentice everything he knew, then
              his apprentice killed him in his sleep. Ironic. He could save
              others from death, but not himself.
              <br />
              <br />
              “Order sixty-six is the climax of the clone wars, it’s not a
              thrilling climax, the clone wars were never an epic struggle, they
              were never intended to be. The clone wars have always been, in and
              of themselves, from their very inception, the revenge of the sith.
              <br />
              <br />
              The clone wars were the perfect Jedi trap, by fighting at all, the
              Jedi lost. War itself pours darkness in the Force, deepening the
              cloud that limits Jedi perception. And the clones have no malice,
              no hatred, not the slightest ill intent that might give warning.
              They are only following orders. Clones open fire, and Jedi die.
              All across the galaxy. All at once.”
              <br />
              <br />
              So to surmise the force sensitivity of the other Jedi had been
              weakened by the darkness of war and on top of that they were taken
              by surprise by the clones they had fought beside for years and
              trusted implicitly.
              <br />
              <br />
              But Yoda was no ordinary Jedi. He was the Grandmaster of the Order
              and one of the most powerful wielders of the force in all of
              history. Even in his weakened state he could still sense the death
              of hundreds of his fellow Jedi, many of whom were his pupils and
              were as close as family. Thus this vast wave of death and pain
              that engulfed him was actually the first warning he received that
              something had gone horribly wrong. Inspite of his personal
              feelings, Yoda did what a good Jedi does and cleared his mind and
              extended his formidable forces senses.
              <br />
              <br />
              So when Clone Commander Gree and his fellows came to assassinate
              Yoda, he was already in a state of high alert. His sharp mind had
              probably already analysed the situation and come up with the only
              reasonable explanation for the mass, near simultaneous death of so
              many Jedi. The Clones had betrayed them. And once the element of
              surprise was lost, the Clones had no chance of defeating Yoda in a
              straight up fight
            </Typography>
          </div>
        </CardContent>
      </Card>
    </Layout>
  );
}

export async function getStaticPaths() {
  const res = await fetch(`http://localhost:8080/v1/graphql`, {
    method: "POST",
    body: JSON.stringify({
      operationName: "GetQuoteIds",
      query: `
        query GetQuoteIds {
          quote {
            id
          }
        }
      `,
    }),
  });
  const response = await res.json();
  const paths = response.data.quote.map((quote) => ({
    params: { id: quote.id.toString() },
  }));

  return {
    paths,
    fallback: false,
  };
}

export async function getStaticProps({ params }) {
  const res = await fetch(`http://localhost:8080/v1/graphql`, {
    method: "POST",
    body: JSON.stringify({
      operationName: "GetQuoteById",
      query: `
        query GetQuoteById($id: Int!) {
          quote_by_pk(id: $id) {
            character
            episode
            id
            quote
          }
        }
      `,
      variables: { id: params.id },
    }),
  });
  const response = await res.json();

  // Pass data to the page via props
  return { props: { quote: response.data.quote_by_pk } };
}
