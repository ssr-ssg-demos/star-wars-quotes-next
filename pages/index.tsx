import Layout from "../components/layout";
import QuoteCard from "../components/quote-card";
import { Mutation_Root } from "../graphql/types";
import gql from "graphql-tag";
import { useMutation } from "@apollo/react-hooks";
import withApollo from "../lib/with-apollo";
import { ApolloClient } from "apollo-boost";

const VOTE_ON_QUOTE = gql`
  mutation voteOnQuote($id: Int!, $increment: Int!) {
    update_quote(where: { id: { _eq: $id } }, _inc: { votes: $increment }) {
      returning {
        id
        votes
      }
    }
  }
`;

function Home({ data }) {
  const [vote] = useMutation<Mutation_Root>(VOTE_ON_QUOTE);
  let quoteCards;

  if (data?.quote)
    quoteCards = data.quote.map((q) => (
      <QuoteCard
        quote={q}
        key={q.id}
        vote={(id, increment) => {
          vote({ variables: { id, increment } });
        }}
      />
    ));

  return (
    <Layout>
      <h1>Vote on Quotes, You Will</h1>
      <div className="card-container">
        <div className="scrollbox">{quoteCards}</div>
        <div className="fade-in" />
        <div className="fade-out" />
      </div>
      <style jsx>
        {`
          h1 {
            margin: auto;
            color: white;
            padding: 3rem 0;
            text-align: center;
          }
          .card-container {
            display: flex;
            position: relative;
            flex-direction: column;
            justify-content: center;
            width: 70%;
            height: 35rem;
            margin: auto;
            text-align: left;
          }
          .scrollbox {
            height: 100%;
            overflow: scroll;
          }
          .fade-out {
            position: absolute;
            z-index: 1;
            bottom: 0;
            left: 0;
            pointer-events: none;
            width: 100%;
            height: 7rem;
            background-image: linear-gradient(
              to bottom,
              rgba(0, 0, 0, 0),
              rgba(0, 0, 0, 1)
            );
          }
          .fade-in {
            position: absolute;
            z-index: 1;
            top: 0;
            left: 0;
            pointer-events: none;
            width: 100%;
            height: 1rem;
            background-image: linear-gradient(
              to bottom,
              rgba(0, 0, 0, 1),
              rgba(0, 0, 0, 0)
            );
          }
        `}
      </style>
    </Layout>
  );
}

// This gets run on the server on initial load,
// and subsequently on the client
export async function getInitialProps(ctx) {
  const apolloClient: ApolloClient<any> = ctx.apolloClient;
  const result = await apolloClient.query({
    query: gql`
      query GetQuotes {
        quote(order_by: { votes: desc }) {
          id
          quote
          character
          votes
          episode
        }
      }
    `,
  });
  return { data: result.data };
}

Home.getInitialProps = getInitialProps;

export default withApollo(Home);
