import Layout from "../components/layout";
import {
  makeStyles,
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Card,
  TablePagination,
} from "@material-ui/core";
import { useState } from "react";
import Link from "next/link";

const useStyles = makeStyles({
  card: {
    margin: "5rem 5rem auto",
  },
  container: {
    maxHeight: 600,
  },
});

function Quotes({ data }) {
  const classes = useStyles();
  const quotes = data.quote;

  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (
    <Layout>
      <Card className={classes.card}>
        <TableContainer className={classes.container}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                <TableCell>Quote</TableCell>
                <TableCell>Character</TableCell>
                <TableCell>Episode</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {quotes
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((quote) => {
                  return (
                    <Link href={`quote/${quote.id}`} key={quote.id}>
                      <TableRow
                        hover
                        role="checkbox"
                        tabIndex={-1}
                        key={quote.id}
                      >
                        <TableCell>{quote.quote}</TableCell>
                        <TableCell>{quote.character}</TableCell>
                        <TableCell>{quote.episode}</TableCell>
                      </TableRow>
                    </Link>
                  );
                })}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={quotes.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Card>
    </Layout>
  );
}

export async function getStaticProps() {
  const res = await fetch(`http://localhost:8080/v1/graphql`, {
    method: "POST",
    body: JSON.stringify({
      operationName: "GetQuotes",
      query: `
        query GetQuotes {
          quote(order_by: {votes: desc}) {
            id
            quote
            character
            episode
          }
        }
      `,
    }),
  });
  const response = await res.json();

  // Pass data to the page via props
  return { props: { data: response.data } };
}

export default Quotes;
